<?php
/**
 * The template for displaying Production page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @author Botez Costin <costibotez94@gmail.com>
 * @package FilterlessCo
 */

get_header(); ?>

	<div id="primary" class="content-area representation">
		<main id="main" class="site-main">
			<div class="container">
				<h1><img style="margin-right: 5px;" src="<?php echo the_post_thumbnail_url(); ?>" alt="representation-vector"/><?php the_title(); ?></h1>
				<p><?php global $post; echo $post->post_content; ?></p>
				<!-- projects -->
   	 			<div class="artists">
					<?php
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

					$args = array(
						'post_type'			=> 'artists',
						// 'orderby' 			=> 'title',
						// 'order'    			=> 'ASC',
						'paged'				=> $paged,
						'posts_per_page'	=> 12
					);

					$the_query = new WP_Query( $args );
					if($the_query->have_posts() ) :
						while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<div class="col-md-3">
								<div class="col-md-12">
									<a href="<?php the_permalink(); ?>">
										<?php
											// if ( has_post_thumbnail() ) {
											//     the_post_thumbnail('projects-size', array('class' => 'img-responsive'));
											// } else {
											// 	//placeholder
											// 	echo '<img class="img-responsive" src="'.home_url('/wp-content/uploads/2016/12/representation-placeholder.jpg').'"/>';
											// }
											$attachments = new Attachments( 'okb_attachments' );
											if( $attachments->exist() ) :
												while( $attachments->get() ) :
													echo $attachments->image( 'represenation-size' );
													break;
												endwhile;
											elseif ( has_post_thumbnail() ):
												echo '<img class="img-responsive" src="';
												echo the_post_thumbnail_url('artists-size');
												echo '" alt="artist-img" />';
											endif;
										?>
										<h2><?php the_title(); ?></h2>
										<h3><?php echo get_field('city'); ?></h3>
									</a>
								</div>
							</div>
						<?php endwhile; ?>
				</div>
				<!-- pagination -->
				<div class="col-md-12">
					<?php costin_pagination($the_query); ?>
				</div>
				<!-- /.pagination -->
				<?php wp_reset_postdata(); ?>
				<?php else:  ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>

			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();

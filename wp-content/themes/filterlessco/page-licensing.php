<?php
/**
 * The template for displaying Licensing page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @author Botez Costin <costibotez94@gmail.com>
 * @package FilterlessCo
 */

get_header(); ?>

	<div id="primary" class="content-area licensing">
		<main id="main" class="site-main">
			<div class="container">
				<h1><img style="margin-right: 5px;" src="<?php echo the_post_thumbnail_url(); ?>" alt="licensing-vector"/><?php the_title(); ?></h1>
				<p><?php global $post; echo $post->post_content; ?></p>
			</div>
			<div>
				<?php $photo_featured = get_field( "photo_featured" ); ?>
				<div class="col-xs-6 col-md-4 col-lg-4 img-responsive">
					<div>
						<img class="img-responsive" src="<?php echo $photo_featured; ?>" alt="photo_featured"/>
					</div>
					<div>
						<?php if(!empty($photo_title = get_field( "photo_title" ))) : ?>
							<h2><?php echo $photo_title; ?></h2>
						<?php endif; ?>
						<?php if(!empty($photo = get_field( "photo" ))) : ?>
							<?php echo $photo; ?>
						<?php endif; ?>
					</div>
				</div>
				<?php $video_featured = get_field( "video_featured" ); ?>
				<div class="col-xs-6 col-md-4 col-lg-4 img-responsive">
					<div>
						<img class="img-responsive" src="<?php echo $video_featured; ?>" alt="video_featured"/>
					</div>
					<div>
						<?php if(!empty($video_title = get_field( "video_title" ))) : ?>
							<h2><?php echo $video_title; ?></h2>
						<?php endif; ?>
						<?php if(!empty($video = get_field( "video" ))) : ?>
							<?php echo $video; ?>
						<?php endif; ?>
					</div>
				</div>
				<?php $music_featured = get_field( "music_featured" ); ?>
				<div class="col-xs-6 col-md-4 col-lg-4 img-responsive">
					<div>
						<img class="img-responsive" src="<?php echo $music_featured; ?>" alt="music_featured"/>
					</div>
					<div>
						<?php if(!empty($music_title = get_field( "music_title" ))) : ?>
							<h2><?php echo $music_title; ?></h2>
						<?php endif; ?>
						<?php if(!empty($music = get_field( "music" ))) : ?>
							<?php echo $music; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();

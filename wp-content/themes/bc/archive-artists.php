<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package okb
 * @since okb 1.0
 */

get_header(); 
$args = array(
  'post_type'   => 'artists', 
  'post_status' => 'publish',
  'numberposts' => -1,
);

$artists = get_posts($args);

?>

<div class='projects'>
  <ul class="projects">
  <?php foreach ($artists as $post): ?>
    <?php echo okb_partial('artist') ?>
  <?php endforeach ?>
  </ul>
</div>

<?php get_footer(); ?>



<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package okb
 * @since okb 1.0
 */

get_header(); ?>

<?php
  global $wp_query;
  $max = $wp_query->max_num_pages;
  $catid =  get_query_var('cat');
  if ($catid) {
    $category = get_category($catid);
    $category = $category->name;
  }
  $text = okb_get_option('blog_page_text','site_general');
?>

    <div class='twelve'>
      <p><?php echo $text ?></p>

      <hr/>
    </div>

    <ul class="entries nine infinite-scrolling" data-category='<?php echo $category ?>' data-maxpages='<?php echo $max ?>' data-url='<?php echo get_template_directory_uri() . '/ajax-load.php'; ?>'>

      <?php if ( have_posts() ) : ?>

        <?php /* Start the Loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>

        <?php echo okb_partial('post') ?>

        <?php endwhile; ?>

      <?php elseif ( current_user_can( 'edit_posts' ) ) : ?>

        <?php get_template_part( 'no-results', 'index' ); ?>

      <?php endif; ?>
    </ul>

<?php get_sidebar(); ?>
<?php get_footer(); ?>


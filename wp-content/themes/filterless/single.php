<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

require_once('models.php');

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} elseif ($post->post_type == 'project') {
	$post = Timber::query_post(false, 'Project');
	$context['post'] = $post;
	Timber::render('project.twig', $context);
} elseif ($post->post_type == 'artists') {
	$post = Timber::query_post(false, 'Artist');
	$context['post'] = $post;
	Timber::render('artist.twig', $context);
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}

<?php
  $attachment = get_attachments($post->ID);
  $attachment = $attachment[0];
  if( $attachment ) {
    $image = $attachment['src'];
    $video = $attachment['video'];
  }
  $classes = '';
  if ($post->post_type == 'project') {
    $classes  = array();
    $services = get_post_meta($post->ID,'services');

    foreach ($services as $service) {
      array_push($classes, 'tag-'.$service['post_name']);
    }
    $classes = ' '. implode(' ', $classes);
  }

?>
<li class="six<?php echo $classes ?>">
  <?php if ($video): ?>
    <div class="embedded-video-wrapper">
      <iframe frameborder="0" height="306" src="<?php echo $video ?>?byline=0&amp;portrait=0&amp;badge=0&amp;color=beb4d7" width="460">webkitAllowFullScreen mozallowfullscreen allowFullScreen</iframe>
    </div>
  <?php else: ?>
    <a href="<?php echo get_permalink($post->ID); ?>">
      <?php if ($image): ?>
        <img class="scale" alt="" width="460" height="306" src="<?php echo $image ?>" />
      <?php else: ?>
        <img class="scale" alt="" width="460" height="306" src="<?php echo get_template_directory_uri(); ?>/images/placeholder/placeholder-700x390_01.png" />
      <?php endif; ?>
      <span class="content">
        <span class="iw">
          <span class="title"><?php echo $post->post_title ?></span>
        </span>
      </span>
    </a>
  <?php endif; ?>
</li>

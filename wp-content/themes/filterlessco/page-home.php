<?php
/**
 * The template for displaying Home page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @author Botez Costin <costibotez94@gmail.com>
 * @package FilterlessCo
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="entry-content">
					<!-- <img src="<?php //echo the_post_thumbnail_url(); ?>" /> -->
					<h1 class="container"><?php global $post; echo $post->post_content; ?></h1>
				</div><!-- .entry-content -->
			</article><!-- #post-## -->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

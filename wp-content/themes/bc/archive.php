<?php
/**
 * The Template for displaying post archives
 *
 * @package okb
 * @since okb 1.0
 */

get_header(); ?>

  <?php while ( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'content', 'single' ); ?>

  <?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>

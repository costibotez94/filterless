<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package okb
 * @since okb 1.0
 */

get_header();

  $args = array(
    'post_type'    => 'any',
    'post_status'  => 'publish',
    'numberposts'  => -1,
    'meta_key'     => '_featured',
    'meta_value'   => 'yes',
  );
  $query = new WP_Query($args);
  $posts =  $query->posts;
?>
    <ul class="projects big">
    <?php foreach ($posts as $post): ?>
      <?php echo okb_partial('project_big') ?>
    <?php endforeach ?>
    </ul>
<?php get_footer(); ?>

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package	WordPress
 * @subpackage	Timber
 * @since		Timber 0.1
 */

class PostWithAttachments extends TimberPost {
	protected $_attachments;
	protected $attachment_key = "okb_attachments";
	public function get_attachments() {
		if (!$this->_attachments) {
			$this->_attachments = array();
			$attachments = new Attachments( $this->attachment_key, $this->id );
			if( $attachments->exist() ) {
				while( $attachment = $attachments->get() ) {
					$obj						= new stdClass();
					$obj->src_med	 = $attachments->src( 'medium' );
					$obj->src_large = $attachments->src( 'large' );
					$obj->src_full	= $attachments->src( 'full' );
					$obj->src			 = $attachments->src( 'original' );
					$obj->video		 = $attachments->field( 'video' );
					$obj->autoplay	= $attachments->field( 'autoplay' );
					$obj->webm			= $attachments->field( 'webm' );
					$obj->mp4			 = $attachments->field( 'mp4' );
					$obj->caption	 = $attachments->field( 'caption' );
					$obj->page_id	 = $attachments->field( 'page_id' );
					$obj->url			 = $attachments->field( 'url' );
					array_push($this->_attachments, $obj);
				}
			}
		}
		return $this->_attachments;
	}
	public function get_image_src($size = 'original'){
		$src = wp_get_attachment_image_src( get_post_thumbnail_id( $this->ID ), $size );
		$src = $src[0];
		if (!$src) {
			$src = $this->get_attachments();
			$src = $src[0] ? $src[0]->src : null;
		}
		return $src;
	}
}


class Artist extends PostWithAttachments {

}

class Project extends PostWithAttachments {

}

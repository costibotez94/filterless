jQuery(document).ready(function($) {
  var mySlider = $('.hero-slider');
  mySlider.unslider({
    arrows: {
      prev: '<a class="unslider-arrow prev"><i class="material-icons">navigate_before</i></a>',
      next: '<a class="unslider-arrow next"><i class="material-icons">navigate_next</i></a>'
    }
  });
});

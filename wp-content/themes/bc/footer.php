<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package okb
 * @since okb 1.0
 */
  $facebook   = trim(okb_get_option('facebook','social'));
  $vimeo      = trim(okb_get_option('vimeo','social'));
  $linkedin   = trim(okb_get_option('linkedin','social'));
  $twitter    = trim(okb_get_option('twitter','social'));
  $instagram  = trim(okb_get_option('instagram','social'));

  $gmap       = trim(okb_get_option('gmap','contact_info'));
  $filterless = trim(okb_get_option('filterless','contact_info'));
  $phone      = trim(okb_get_option('phone','contact_info'));
  $address    = trim(okb_get_option('address','contact_info'));
  $missionst  = trim(okb_get_option('mission_statement','contact_info'));
?>

  </div><!-- #main -->

  <footer id='footer' class='container'>
      <div class="twelve" id="colophon">
        <p>
          &copy; 2013 Filterless - All Rights Reserved | a <a href='http://butchershop.co/'>Butchershop Creative</a> Company
        </p>
      </div>

  </footer>

</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>
<script src="http://platform.tumblr.com/v1/share.js"></script>
</body>
</html>

<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package FilterlessCo
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( !is_single() ) :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php filterlessco_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<!-- <div class="col-md-12 featured-responsive">
		<?php //if(in_array(get_post_type(), array('artists', 'project'))) : ?>
			<?php //$attachments = new Attachments( 'okb_attachments' ); ?>
			<?php //if( $attachments->exist() ) : ?>
			    <?php //while( $attachments->get() ) : ?>
	        		<?php //echo $attachments->image( 'full' ); ?>
	        		<?php //break; ?>
			  	<?php //endwhile; ?>
			<?php //elseif ( has_post_thumbnail() ): ?>
				<?php //echo '<img class="img-responsive" src="'.get_the_post_thumbnail_url().'" alt="artist-img"/>'; ?>
			<?php ///endif; ?>
		<?php //endif; ?>
		</div> -->
		<div class="col-md-12">
			<?php if ( is_single() ) :
					if((get_post_type() == 'artists')) :
						the_title( '<h1 class="entry-title">', '</h1>' );
						if(!empty($city = get_field('city'))) :
							echo "<h2 class='artist-city'>$city</h2>";
						endif;
					else:
						if(!empty($client = get_field('client'))) :
							echo "<h1>CLIENT <strong>/ $client[post_title]</strong></h1>";
						endif;
						if(!empty($metadatas = get_field('metadata')) ) :
							foreach ($metadatas as $metadata) :
								echo "<h2>$metadata[label] / <strong>$metadata[value]</strong></h2>";
							endforeach;
						endif;
					endif;
			endif; ?>
			<?php
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'filterlessco' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
			?>
		</div>
<!-- 		<div class="col-md-7 min980">
		<?php //if(in_array(get_post_type(), array('artists', 'project'))) : ?>
			<?php //$attachments = new Attachments( 'okb_attachments' ); ?>
			<?php //if( $attachments->exist() ) : ?>
			    <?php //while( $attachments->get() ) : ?>
	        		<?php //echo $attachments->image( 'full' ); ?>
	        		<?php //break; ?>
			  	<?php// endwhile; ?>
			<?php// elseif ( has_post_thumbnail() ): ?>
				<?php //echo '<img class="img-responsive" src="'.get_the_post_thumbnail_url().'" alt="artist-img"/>'; ?>
			<?php //endif; ?>
		<?php// endif; ?>
		</div> -->
		<div class="col-md-12">
		<?php if(in_array(get_post_type(), array('artists', 'project'))) : ?>
			<?php $attachments = new Attachments( 'okb_attachments' ); ?>
			<?php if( $attachments->exist() ) : ?>
			    <?php while( $attachments->get() ) : ?>
			  		<div class="col-xs-12 col-md-4 col-lg-4 item">
			        	<a>
			        		<?php
			        			$height = $attachments->height( 'full' );
			        			$width = $attachments->width( 'full' );
			        			if($width >= $height) {
			        				echo '<img src="' . esc_url( $attachments->src('full') ) . '" class="horizontal"/>';
			        			} else {
			        				echo '<img src="' . esc_url( $attachments->src('full') ) . '" height="270" class="vertical"/>';

			        			}
			        		?>
			        		<?php //echo $attachments->image( 'full' ); ?>
			        	</a>
			  		</div>
			    <?php endwhile; ?>
			<?php endif; ?>
		<?php endif; ?>
		</div>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
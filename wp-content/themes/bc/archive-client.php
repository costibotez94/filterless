<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package okb
 * @since okb 1.0
 */

get_header(); 


$clients = get_posts(array(
  'post_type'   => 'client', 
  'post_status' => 'publish',
  'orderby' => 'title',
  'order'   => 'ASC',
  'numberposts' => -1,
));

$text = okb_get_option('client_page_text','site_general');

?>

<div class="twelve"></div>
<p>
<?php echo $text ?>
</p>

<p class="clients">
<?php
    foreach( $clients as $client ) {

      $this_char = strtoupper(mb_substr($client->post_title, 0, 1, 'UTF-8'));

      if (strpos('0123456789',$this_char) !== false) $this_char = '0-9';

      if ($this_char != $last_char) {
        $last_char = $this_char;
        $html .= '<strong>('.$last_char.') </strong>';
        $html .= project_archive_link($client);
      } else {
        $html .= project_archive_link($client);
      }
    }
    echo $html;
?>
</p>

<?php get_footer(); ?>

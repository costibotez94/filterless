<?php
  // http://wp.tutsplus.com/tutorials/getting-loopy-ajax-powered-loops-with-jquery-and-wordpress/
  define('WP_USE_THEMES', false);
  require_once('../wordpress/wp-load.php');

  $args = array(
     'posts_per_page' => get_option('posts_per_page '),
     'paged'          => isset($_GET['page']) ? $_GET['page'] : 1
  );

  if (isset($_GET['cat'])) {
    $args['category_name'] = $_GET['cat'];
  }

  query_posts($args);

?>

<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <div class='post-container<?php if (!is_single()) { echo ' five alpha'; }?>'>
    <?php get_template_part( 'content', get_post_format() ); ?>
    </div>
  <?php endwhile; ?>
<?php else: ?>
  <?php header('HTTP/1.0 404 Not Found'); exit() ?>
<?php endif; ?>

<?php wp_reset_query(); ?>

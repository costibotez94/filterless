<?php

$services = get_posts(array(
  'post_type'   => 'service',
  'post_status' => 'publish',
  'sort_order'  => 'desc',
  'sort_column' => 'post_title',
  'numberposts' => -1,
  'tax_query' => array(
      array(
        'taxonomy' => 'service_categories',
        'field' => 'id',
        'terms' => 3
      )
    ),
));

?>

<div class="filter twelve">
  <ul>
    <li><a href="#!" data-filter='*'>ALL</a></li>
  <?php foreach ($services as $service): ?>
    <li><a href='#!' data-filter='.<?php echo 'tag-'.$service->post_name ?>'><?php echo $service->post_title ?></a></li>
  <?php endforeach ?>
  </ul>
</div>

<?php
  $url = urlencode(get_permalink());
  $title = urlencode(get_the_title());
  $source = urlencode( get_bloginfo( 'name', 'display' ));
?>

<li class='facebook'><a href='http://www.facebook.com/sharer.php?<?php echo "u=$url&t=$title" ?>'>Share on Facebook</a></li>
<li class='linkedin'><a href='http://www.linkedin.com/shareArticle<?php echo "?mini=true&url=$url&title=$title&source=$source" ?>'>Share on LinkedIn</a></li>
<li class='google-plus'><a href='https://plus.google.com/share?<?php echo "url=$url" ?>'>Share on Google Plus</a></li>
<li class='twitter'><a href='http://twitter.com/share?<?php echo "url=$url&text=$title" ?>'>Share on Twitter</a></li>

<?php

// TODO: Check out http://photoswipe.com/

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	} );
	return;
}

Timber::$dirname = array('templates', 'views');

function post_options($type = 'post'){
  $query = new wp_query(array(
    'post_type'      => $type,
    'posts_per_page' => -1,
    'post_status'=>'publish',
    'nopaging'       => true,
  ));
  $posts =  $query->posts;
  $ret  = array('' => '');
  foreach ($posts as $post) {
    $ret[$post->ID] =  $post->post_title;
  }
  return $ret;
}

function okb_attachments( $attachments ) {
  $fields         = array(
    array(
      'name'      => 'caption',
      'type'      => 'textarea',
      'label'     => __( 'Caption', 'attachments' ),
      'default'   => '',
    ),
    // array(
    //   'name'      => 'bg_color',
    //   'type'      => 'text',
    //   'label'     => __( 'Background Color', 'attachments' ),
    //   'default'   => 'rgba(255,255,255,1)',
    // ),
    array(
      'name'      => 'page_id',
      'type'      => 'select',
      'label'     => __( 'Page Link', 'attachments' ),
      'meta'      => array(
        'multiple'      => false,
        'options'       => post_options(array('artists','project','post'))
      ),
    ),
    array(
      'name'      => 'url',
      'type'      => 'text',
      'label'     => __( 'External URL', 'attachments' ),
    ),
    array(
      'name'      => 'video',
      'type'      => 'text',
      'label'     => __( 'Video Embed URL', 'attachments' ),
      'desc'      => 'This will override the image if both are set'
    ),
    array(
      'name'      => 'credit',
      'type'      => 'text',
      'label'     => __( 'Photo Credit', 'attachments' ),
    ),
  );

  $args = array(
    // title of the meta box (string)
    'label'         => 'Slides',

    // all post types to utilize (string|array)
    'post_type'     => array( 'slideshow', 'project', 'artists', 'post' ),

    // meta box position (string) (normal, side or advanced)
    'position'      => 'normal',

    // meta box priority (string) (high, default, low, core)
    'priority'      => 'high',

    // allowed file type(s) (array) (image|video|text|audio|application)
    'filetype'      => array('image'),

    // by default new Attachments will be appended to the list
    // but you can have then prepend if you set this to false
    'append'        => true,

    // text for 'Attach' button in meta box (string)
    'button_text'   => __( 'Attach Files', 'attachments' ),

    // text for modal 'Attach' button (string)
    'modal_text'    => __( 'Attach', 'attachments' ),

    // which tab should be the default in the modal (string) (browse|upload)
    'router'        => 'browse',

    // fields array
    'fields'        => $fields,

  );

  $attachments->register( 'okb_attachments', $args ); // unique instance name

}

add_action( 'attachments_register', 'okb_attachments' );


class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );

		add_filter('timber_context', array( $this, 'add_to_context'));
		add_filter('get_twig', array( $this, 'add_to_twig'));

		add_action('init', array( $this, 'register_post_types'));
		add_action('init', array( $this, 'register_taxonomies'));
		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));

		$this->configure_options_page();
		$this->configure_image_sizes();

		parent::__construct();
	}

	function enqueue_scripts() {
		wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=Julius+Sans+One|Jura|Questrial|Quicksand|Oswald|Roboto|Roboto+Condensed|Roboto+Slab',
										array(), '', false);
		wp_enqueue_style('fonts-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons',
										array('fonts'), '', false);
		wp_enqueue_style('foundation-css',
										'https://cdn.jsdelivr.net/foundation/6.2.3/foundation.min.css',
										array('fonts'));
		wp_enqueue_style('filterless-css', get_template_directory_uri() . '/filterless.css',
										array('fonts', 'foundation-css'), '1.4');
		wp_enqueue_style('unslider-dots-css', get_template_directory_uri() . '/static/unslider-dots.css',
										array('foundation-css'), '1.0');
		wp_enqueue_style('unslider-css', get_template_directory_uri() . '/static/unslider.css',
										array('foundation-css'), '1.0');
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js',
											array(), '2.1.0', false);
		wp_enqueue_script('jquery', '//npmcdn.com/object-fit-images@2.5.3', array(), '2.1.0', false);
		wp_enqueue_script('what-input', 'https://cdn.jsdelivr.net/what-input/2.1.1/what-input.min.js',
											'2.1.1', false);
		wp_enqueue_script('foundation-js',
											'https://cdn.jsdelivr.net/foundation/6.2.3/foundation.min.js',
											array('jquery', 'what-input'), '1', true);
		wp_enqueue_script('unslider',
											get_template_directory_uri() . '/static/unslider-min.js',
											array('foundation-js'), '1', true);
		wp_enqueue_script('masonry',
											'https://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js',
											array('unslider'), '1', true);
		wp_enqueue_script('unslider-toggle',
											get_template_directory_uri() . '/static/filterless.js',
											array('masonry'), '1', true);
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		$context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['menu'] = new TimberMenu();
		$context['site'] = $this;
		$context['options'] = get_fields('option');
		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

	function configure_options_page() {
		if (function_exists('acf_add_options_page')) {
			acf_add_options_page();
		}
	}

	function configure_image_sizes() {
		// add_image_size('artist', 370, 247, array('center', 'center'));
	}

}

new StarterSite();

<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package okb
 * @since okb 1.0
 */

get_header();
$args = array(
  'post_type'   => 'project',
  'post_status' => 'publish',
  'numberposts' => -1,
);

if ($_GET['cid']) {
  $args['meta_key']   = 'client';
  $args['meta_value'] = (int)$_GET['cid'];
} elseif ($_GET['sid']) {
  $args['meta_key']   = 'services';
  $args['meta_value'] = (int)$_GET['sid'];
}

$projects = get_posts($args);

?>

<div class='projects isotope'>
  <?php okb_partial('filter') ?>
  <ul class="projects twelve">
  <?php foreach ($projects as $post): ?>
    <?php echo okb_partial('project') ?>
  <?php endforeach ?>
  </ul>
</div>

<?php get_footer(); ?>



<li class="row">
  <div class="six alpha entry-image">
    <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'okb' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
      <?php if (has_post_thumbnail()): ?>
        <?php the_post_thumbnail(array(460,306), array('class' => 'scale')); ?>
      <?php else: ?>
        <img class="scale" alt="" width="460" height="306" src="<?php echo get_template_directory_uri(); ?>/images/placeholder/placeholder-460x260.png" />
      <?php endif; ?>
    </a>
  </div>

  <div class="three omega">
    <h2 class="entry-title">
      <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'okb' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
    </h2>
    <div class="entry-meta">
      <span class="entry-date"><?php okb_posted_on(); ?></span> - <a class='entry-author' href='<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?> '><?php echo get_the_author() ?></a>
    </div>
    <div class="entry-content">
      <?php the_excerpt(); ?>
    </div>
   <?php echo get_the_tag_list('<ul class="entry-tags"><li>','</li><li>','</li></ul>'); ?>
  </div>
</li>



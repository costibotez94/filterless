(function($){
  // Scoping to no-js will happen sooner than scoping to js-enabled
  $('html').removeClass('no-js').addClass('js-enabled');
  
  window.App = {
    init: function(){
      // Fallback to normal animation if CSS3 unavailable
      if (!$.support.transition) $.fn.transition = $.fn.transit = $.fn.animate;
      $.each(App.ui,function(k,fn){ fn(); });
    },
    state: {
      device    : function(){
        if (window.getComputedStyle)
          return window.getComputedStyle(document.body,':after').getPropertyValue('content').replace(/['"]/g,'');
      },
      isPhone        : function(){ return this.device() == 'phone';   },
      isTablet       : function(){ return this.device() == 'tablet';  },
      isDesktop      : function(){ return $.inArray(this.device(),['', 'none', 'desktop-large']) > -1; },
      isDesktopLarge : function(){ return this.device() == 'desktop-large'; }
    },
    ui: {
      slideshows: function(){
        $(".slideshow").each(function(){
          var isBg = $(this).attr('id') == 'background-slideshow',
              ui = ['navigation', 'touch', 'currentPage'];

          ui.push((isBg ? 'delayedCaption' : 'caption'));

          $(this).okCycle({
            effect: isBg ? 'slide' : 'fade',
            easing: 'easeInOutExpo',
            eagerLoad: isBg ? 20 : 1,
            ui: ui,
            autoplay: isBg ? 3000 : false,
            speed: isBg ? 1000 : 300,
            duration: 6000,
            onLoad      : function(slideshow, imageData){
              imageData.img.css('background-image','url('+imageData.img.data('src')+')').fadeTo.apply(imageData.img, imageData.isLoaded ? ['fast',1] : [0,0]);
            },
            afterSetup: function(s){
              s.parent().addClass(s.attr('id'));
              var html = '<div class="loader"><div class="duo duo1"><div class="dot dot-a"></div><div class="dot dot-b"></div></div><div class="duo duo2"><div class="dot dot-a"></div><div class="dot dot-b"></div></div></div>';
              $(html).insertBefore(s);
            }
          });
        });

        $("#quotes ul").okCycle({
          effect: 'scroll',
          autoplay: 3000,
          duration: 5000,
          hoverBehavior : function(slideshow){
            var api = $(slideshow).okCycle();
            $("#quotes").hover(function(){ api.pause(); }, function(){ api.play();  });
          },
          afterSetup: function(slideshow){
            var api = $(slideshow).okCycle();
            $("#quotes a.next").click(function(e){ e.preventDefault(); api.next(); });
          }
        });
      },
      isotope: function(){
        var container = $(".isotope"),
            filter = $('.projects .filter a');

        container.isotope({
          itemSelector : '.six'
        });

        filter.on('click.ui', function(e){
          var self     = $(this),
              selector = self.attr('data-filter');

          self.closest('li').addClass('active').siblings().removeClass('active');
          container.isotope({ filter: selector });
          return false;
        });

        // Safari does dumb things
        $( window ).load( function() {
          if (container.hasClass('isotope')) container.isotope('reLayout');
        });

      },
      infiniteScrolling: function(){

        $(".infinite-scrolling").each(function(){
          var container = $(this),
              category  = container.data('category'),
              url       = container.data('url'),
              maxpage   = container.data('maxpages');

          $(document).miniScroll({
            url: function(count){
              return  url + "?page=" + count + ( category ? "&cat=" + category : '' );
            },
            buffer: 400,
            container: container,
            onSuccess: function(data){
              $(data.html).appendTo(this.opts.container).hide().fadeIn();
            },
            dataType: 'html',
            onLoad: function(){
              var loading = $("#live-loading");
              if (loading.length === 0) {
                loading = $("<div id='live-loading' class='loading'>Loading</div>").insertAfter(this.opts.container).hide();
              }
              loading.fadeIn();
            },
            onComplete: function(){
              App.ui.slideshows();
              $("#live-loading").fadeOut();
            },
            onFinish: function(){
              $("<li class='no-results'>No more results</li>").appendTo(container);
            }
          });
        });
      },
      tabs: function(){
        $(".tabs").okNav({history: false, event: 'click mouseenter' });
      },
      contact: function(){
        $("body").on("contact_success",function(e){
        });
      }
    }
  };

  $(function(){ App.init(); });

})(jQuery);

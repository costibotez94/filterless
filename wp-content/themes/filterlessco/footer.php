<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @author Botez Costin <costibotez94@gmail.com>
 * @package FilterlessCo
 */

?>

	</div><!-- #content -->
	<?php if(is_page('contact')) : ?>
	<footer id="colophon" class="site-footer">
		<div class="container">
			<a href="<?php echo home_url(); ?>">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer-logo.png" alt="footer-logo" />
			</a>
			<p><?php _e( 'Copyright © 2017 filterlessco', 'filterlessco' ); ?></p>
			<?php if ( dynamic_sidebar('footer-social-icons') ) : else : endif; ?>
		</div>
	</footer><!-- #colophon -->
	<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package okb
 * @since okb 1.0
 */
?>
<!doctype html>
<!--[if LT IE 8]><html class="ie8 oldie ancientie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]><html class="ie9 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html <?php language_attributes(); ?>><!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
  /*
   * Print the <title> tag based on what is being viewed.
   */
  global $page, $paged;

  wp_title( '|', true, 'right' );

  // Add the blog name.
  bloginfo( 'name' );

  // Add the blog description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) )
    echo " | $site_description";

  // Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 )
    echo ' | ' . sprintf( __( 'Page %s', 'okb' ), max( $paged, $page ) );

  ?></title>

  <link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
  <script type="text/javascript" src="//use.typekit.net/fbq1ncs.js"></script>
  <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

  <?php wp_head(); ?>

  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <!--[if lt IE 9]>
  <script src="<?php echo get_template_directory_uri(); ?>/js/lib/html5.js" type="text/javascript"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/lib/selectivizr-min.js" type="text/javascript"></script>
  <style>
    #background-slideshow li {
      -ms-behavior: url(<?php echo get_template_directory_uri(); ?>/js/ie/backgroundsize.min.htc);
    }
  </style>
  <![endif]-->

</head>
<body <?php body_class(); ?>>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-51088-42', 'filterless.co');
  ga('send', 'pageview');
</script>

  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>

<?php if(is_front_page()): ?>
  <h1 id='logo-big'><?php bloginfo('name') ?></h1>
  <h3 id='logo-small'><?php bloginfo('name') ?></h3>
  <?php okb_slideshow(okb_get_option('home_slideshow','site_general')) ?>
  <div id='home-text'>
    <div class='container'>
     <div class='twelve'><?php echo apply_filters('the_content', $post->post_content); ?></div>
    </div>
  </div>
<?php elseif(is_post_type_archive('crew_member')): ?>
  <?php okb_slideshow(okb_get_option('crew_slideshow','site_general')) ?>
<?php endif; ?>

<div id="doc">
  <?php do_action( 'before' ); ?>
  <header id="header" role="banner">
    <nav>
      <div class="container">
        <h1 class="logo one">
          <a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
        </h1>
        <div class='eleven'><?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?></div>
      </div>
    </nav>
  </header><!-- #header -->

  <div id="main" class='container'>

<?php
/**
 * okb functions and definitions
 *
 * @package okb
 * @since okb 1.0
 */

if ( ! function_exists( 'okb_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @since okb 1.0
 */
function okb_setup() {

  /**
   * Custom template tags for this theme.
   */
  require( get_template_directory() . '/inc/template-tags.php' );

  /**
   * Site Customizations
   */
  require( get_template_directory() . '/inc/featured-posts.php' );
  require( get_template_directory() . '/inc/settings.php' );
  require( get_template_directory() . '/inc/shortcodes/services.php' );

  /**
   * Add default posts and comments RSS feed links to head
   */
  add_theme_support( 'automatic-feed-links' );

  /**
   * Enable support for Post Thumbnails
   */
  add_theme_support( 'post-thumbnails' );
  add_image_size( 'medium', 460, 306, true );
  add_image_size( 'large', 700, 466, false );
  add_image_size( 'full', 940, 0, false );

  /**
   * This theme uses wp_nav_menu() in one location.
   */
  register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'okb' ),
    'footer' => __( 'Footer Menu', 'okb' ),
  ) );

  /**
   * Add support for the Aside Post Formats
   */
  add_theme_support( 'post-formats', array( 'aside', ) );

  // Enable Link Manager
  add_filter( 'pre_option_link_manager_enabled', '__return_true' );
}
endif; // okb_setup
add_action( 'after_setup_theme', 'okb_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since okb 1.0
 */
function okb_widgets_init() {
  register_sidebar( array(
    'name' => __( 'Sidebar', 'okb' ),
    'id' => 'sidebar-1',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => "</aside>",
    'before_title' => '<h1 class="widget-title">',
    'after_title' => '</h1>',
  ) );

  register_sidebar( array(
    'name' => __( 'Homepage', 'okb' ),
    'id' => 'sidebar-2',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
  ) );
}
add_action( 'widgets_init', 'okb_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function okb_scripts() {
  global $post;

  wp_enqueue_style( 'grid_12', get_template_directory_uri() . '/css/grid_12.css' );
  wp_enqueue_style( 'grid_12_resp', get_template_directory_uri() . '/css/grid_12_responsive.css' );
  wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize.css' );
  wp_enqueue_style( 'style', get_stylesheet_uri() );

  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }

  wp_enqueue_script("imagesloaded", get_template_directory_uri() . "/js/lib/imagesloaded.js", array(), false, true);
  wp_enqueue_script("transit", get_template_directory_uri() . "/js/lib/jquery.transit.js", array(), false, true);
  wp_enqueue_script("easing", get_template_directory_uri() . "/js/lib/jquery.easing.1.3.js", array(), false, true);
  wp_enqueue_script("isotope", get_template_directory_uri() . "/js/lib/jquery.isotope.js", array(), false, true);

  wp_enqueue_script("okNav", get_template_directory_uri() . "/js/lib/jquery.okNav.js", array(), false, true);
  wp_enqueue_script("okNav.ui", get_template_directory_uri() . "/js/lib/jquery.okNav.ui.js", array(), false, true);
  wp_enqueue_script("miniScroll", get_template_directory_uri() . "/js/lib/jquery.miniScroll.js", array(), false, true);
  wp_enqueue_script("okCycle.core", get_template_directory_uri() . "/js/lib/jquery.okCycle.core.js", array(), false, true);
  wp_enqueue_script("okCycle.ui", get_template_directory_uri() . "/js/lib/jquery.okCycle.ui.js", array(), false, true);
  wp_enqueue_script("okCycle.trans", get_template_directory_uri() . "/js/lib/jquery.okCycle.transitions.js", array(), false, true);

  wp_enqueue_script("application.ui", get_template_directory_uri() . "/js/application/ui.js", array(), false, true);
  wp_enqueue_script("application", get_template_directory_uri() . '/js/application.js', array(), false, true );
}

add_action( 'wp_enqueue_scripts', 'okb_scripts' );


function add_fb_open_graph_tags() {
  $image = 'images/logo.png';

  if (is_single()) {
    global $post;
    if(get_the_post_thumbnail($post->ID, 'thumbnail')) {
      $thumbnail_id = get_post_thumbnail_id($post->ID);
      $thumbnail_object = get_post($thumbnail_id);
      $image = $thumbnail_object->guid;
    }
    $description = og_excerpt( $post->post_content, $post->post_excerpt );
    $description = strip_tags($description);
    $description = str_replace("\"", "\'", $description);
    $title       = get_the_title();
    $type        = 'article';
    $url         = get_permalink();
  } else {
    $url         = get_site_url();
    $title       = get_bloginfo('name');
    $description = get_bloginfo('description');
    $type        = 'website';
    $image       = 'images/logo.jpg';
  }

  ?>

  <meta property="og:title" content="<?php echo $title; ?>" />
  <meta property="og:type" content="<?php echo $type ?>" />
  <meta property="og:image" content="<?php echo $image; ?>" />
  <meta property="og:url" content="<?php  echo $url ?>" />
  <meta property="og:description" content="<?php echo $description ?>" />
  <meta property="og:site_name" content="<?php echo get_bloginfo('name'); ?>" />

  <?php
}

add_action('wp_head', 'add_fb_open_graph_tags');

function og_excerpt($text, $excerpt){

  if ($excerpt) return $excerpt;

  $text = strip_shortcodes( $text );

  $text = apply_filters('the_content', $text);
  $text = str_replace(']]>', ']]&gt;', $text);
  $text = strip_tags($text);
  $excerpt_length = apply_filters('excerpt_length', 55);
  $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
  $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
  if ( count($words) > $excerpt_length ) {
    array_pop($words);
    $text = implode(' ', $words);
    $text = $text . $excerpt_more;
  } else {
    $text = implode(' ', $words);
  }

  return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}

// Rails style partials
function okb_partial($name){
  $chunks = explode('/', $name);
  $last   = array_pop($chunks);

  if ($last[0] != '_') { $last = '_' . $last; }

  array_push($chunks,$last);

  return get_template_part(implode('/', $chunks));
}

function okb_excerpt_more( $more ) {
  return ' <a class="more" href="'. get_permalink( get_the_ID() ) . '">[MORE]</a>';
}
add_filter( 'excerpt_more', 'okb_excerpt_more' );


// Create a slideshow
function okb_slideshow($id, $args = array()) {
  $attachments = new Attachments( 'okb_attachments', $id );
  $count = 0;

  if( $attachments->exist() ) {
    while( $attachment = $attachments->get() ) {
      $src = $attachments->src( '' );
      $credit   = $attachments->field( 'credit' );
      $page_id  = $attachments->field( 'page_id' );

      // Split and wrap each newline in a span
      $caption = okb_break_and_wrap($attachments->field('caption'));

      $url = $attachments->field( 'url' );

      if (!$url && $page_id) {
        $url = get_page_uri($page_id);
      }

      $caption = htmlspecialchars(json_encode(array(
        'caption' => $caption,
        'url'     => $url,
        'credit'  => $credit,
      )), ENT_QUOTES, 'UTF-8');

      $html .= "<li class='slide-$count' data-src='$src' data-caption='$caption'></li>";
      $count++;
    }
  } else {
    echo "<h1>Attachments Not Defined</h1>";
  }

  if (isset($html)) {
    echo "<ul id='background-slideshow' class='slideshow'>$html</ul>";
  }
}

function get_attachments_src($id, $size = 'medium', $type = 'okb_attachments'){
  $attachments = new Attachments( $type, $id );
  $ret = array();

  if( $attachments->exist() ) {
    while( $attachment = $attachments->get() ) {
      array_push($ret, $attachments->src($size));
    }
  }
  return $ret;
}

function get_attachments($id, $size = 'medium'){
  $attachments = new Attachments( 'okb_attachments', $id );
  $ret = array();

  if( $attachments->exist() ) {
    while( $attachment = $attachments->get() ) {
      array_push($ret,
        array(
          'src'      => $attachments->src($size),
          'bg_color' => $attachments->field( 'bg_color' ),
          'caption'  => $attachments->field('caption'),
          'video'    => okb_format_and_extract_video_url($attachments->field('video')),
        )
      );
    }
  }

  return $ret;
}

function okb_break_and_wrap($str){
  $chunks = explode('\n', $str);
  $ret    = array();
  return implode('<br/>', $chunks);
}


add_filter('wp_generate_tag_cloud', 'okb_tag_cloud',10,3);

function okb_tag_cloud($tag_string){
   return preg_replace("/style='font-size:.+pt;'/", '', $tag_string);
}

function fix_blog_menu_css_class( $classes, $item ) {
  if (
    is_singular( 'project' )     || is_post_type_archive( 'project' ) ||
    is_singular( 'crew_member' ) || is_post_type_archive( 'crew_member' ) ||
    is_singular( 'service' )     || is_post_type_archive( 'service' ) ||
    is_singular( 'client' )      || is_post_type_archive( 'client' )
  ) {
        if ( $item->object_id == get_option('page_for_posts') ) {
            $key = array_search( 'current_page_parent', $classes );
            if ( false !== $key )
                unset( $classes[ $key ] );
        }
    }

    return $classes;
}
add_filter( 'nav_menu_css_class', 'fix_blog_menu_css_class', 10, 2 );


function okb_add_api_code($str){
  if (preg_match('/^<iframe/', $str)) {
    $doc = DOMDocument::loadHTML($str);
    $iframes = $doc->getElementsByTagName('iframe');

    foreach ($iframes as $iframe){
      $w = $iframe->getAttribute('width');
      $h = $iframe->getAttribute('height');
      $src = $iframe->getAttribute('src');

      $chunks = preg_split('/\?/', $src);

      $url = $chunks[0] . '?api=1&player_id=player';

      return("<iframe id='player' src='$url' height='$h' width='$w' allowfullscreen='' frameborder='0'></iframe>") ;
    }
  } else {
    return "<iframe id='player' src='$str' height='100%' width='100%' allowfullscreen='' frameborder='0'></iframe>" ;
  }
}

// Return embed code
function okb_format_and_extract_video_url($str) {
  $id = null;

  // Extract from iFrame
  if (preg_match('/^<iframe/', $str)) {
    $doc = DOMDocument::loadHTML($str);
    $iframes = $doc->getElementsByTagName('iframe');

    foreach ($iframes as $iframe){
      $w = $iframe->getAttribute('width');
      $h = $iframe->getAttribute('height');
      $src = $iframe->getAttribute('src');

      $chunks = preg_split('/\?/', $src);

      $str = $chunks[0];
    }
  }

  // Extract Youtube
  preg_match("/.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?\"']*).*/", $str, $matches);

  if (isset($matches[1])) {
    $id = $matches[1];
  }

  if ($id) {
    return "http://www.youtube.com/embed/$id";
  }

  // Extract Vimeo. A little more difficult since the images are stored on AWS
  preg_match("/.*(?:vimeo.com\/video\/)(\d+).*/", $str, $matches);

  if (isset($matches[1])) {
    $id = $matches[1];
  }

  if (!$id) {
    preg_match("/.*(?:vimeo.com\/)(\d+).*/", $str, $matches);
    $id = $matches[1];
  }

  if ($id) {
    return "//player.vimeo.com/video/$id";
  }

  return null;
}


function project_archive_link($client, $txt = null) {
  $txt = $txt ? $txt : get_class($client) ? $client->post_title : $client['post_title'];
  return '<a href="'. get_post_type_archive_link( 'project' ) . '?cid=' . (get_class($client) ? $client->ID : $client['ID']) .'">'. $txt .'</a> ';
}

function project_service_archive_link($service, $txt = null) {
  $txt = $txt ? $txt : get_class($service) ? $service->post_title : $service['post_title'];
  return '<a href="'. get_post_type_archive_link( 'project' ) . '?sid=' . (get_class($service) ? $service->ID : $service['ID']) .'">'. $txt .'</a> ';
}

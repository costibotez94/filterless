<?php
/**
 * The Template for displaying all single posts.
 *
 * @package okb
 * @since okb 1.0
 */

get_header(); 
$email    = get_post_meta(get_the_ID(),'email', true);
$instagram= get_post_meta(get_the_ID(),'instagram', true);
$linkedin  = get_post_meta(get_the_ID(),'linkedin', true);
$facebook = get_post_meta(get_the_ID(),'facebook', true);
$position = get_post_meta(get_the_ID(),'position', true);
$images   = get_attachments_src(get_the_ID(), 'full');
$crews    = get_posts(array(
  'post_type'   => 'crew_member', 
  'post_status' => 'publish',
  'numberposts' => -1,
  'exclude'     => array(get_the_ID())
));

?>

  <?php while ( have_posts() ) : the_post(); ?>

      <div class="crew-member twelve">
        <header>
          <div class='row'>
            <div class='nine alpha crew-member-name'>
              <h2><span class='label'>Name:</span><?php the_title(); ?></h2>
            </div>
            <div class='three omega crew-member-email'>
              <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
            </div>
          </div>
          <div class='row'>
            <div class='six alpha crew-member-position'>
              <h3><span class='label'>Position:</span><?php echo $position ?></h3>
            </div>
            <div class='four'>
              <ul class='crew-member-social'>
                <?php if ($linkedin): ?>
                <li><a href="<?php echo $linkedin ?>">LinkedIn</a></li>
                <?php endif ?>
                <?php if ($instagram): ?>
                <li><a href="<?php echo $instagram ?>">Instagram</a></li>
                <?php endif ?>
                <?php if ($facebook): ?>
                <li><a href="<?php echo $facebook ?>">Facebook</a></li>
                <?php endif ?>
              </ul>
            </div>
            <div class='two omega next-crew'>
              <?php echo next_post_link('%link', 'Next Crew') ?>
            </div>
          </div>
        </header>

        <div class="crew-member-description">
          <?php the_content(); ?>
        </div>
        <ul>
        <?php foreach ($images as $image): ?>
          <li><img class="scale" alt="<?php the_title(); ?>" width="940" height="529" src="<?php echo $image ?>" /></li>
        <?php endforeach ?>
        </ul>
      </div>
      <br class="clear" />
      <ul class="crew">
        <?php foreach ($crews as $post): ?>
          <?php echo okb_partial('project') ?>
        <?php endforeach ?>
      </ul>

  <?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>

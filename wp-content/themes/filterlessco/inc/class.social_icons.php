<?php

/**
 * Social Icons Widget
 *
 * Shows all social icons in footer
 *
 * @author Botez Costin
 * @version 1.0
 */
class Filterlessco_Social_Icons extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'filterlessco_social_icons',
			__('FilterlessCo Social Icons', 'filterlessco'),
			array(
				'description' => __( 'Output all social media links', 'filterlessco' ),
			)
		);
	}

	public function widget( $args, $instance ) {
		echo $args['before_widget']; ?>
		<ul>
		<?php
		if ( ! empty( $instance['facebook'] )  &&  ! empty( $instance['facebook_text'] ) ) : ?>
			<li>
				<a href="<?php echo apply_filters( 'facebook', $instance['facebook'] ); ?>" target="_blank"><i class="fa fa-facebook"></i><?php echo apply_filters( 'facebook_text', $instance['facebook_text'] ); ?></a>
			</li>
		<?php endif;
		if ( ! empty( $instance['twitter'] )  &&  ! empty( $instance['twitter_text'] ) ) : ?>
			<li>
				<a href="<?php echo apply_filters( 'twitter', $instance['twitter'] ); ?>" target="_blank"><i class="fa fa-twitter"></i><?php echo apply_filters( 'twitter_text', $instance['twitter_text'] ); ?></a>
			</li>
		<?php endif;
		if ( ! empty( $instance['instagram'] )  &&  ! empty( $instance['instagram_text'] ) ) : ?>
			<li>
				<a href="<?php echo apply_filters( 'instagram', $instance['instagram'] ); ?>" target="_blank"><i class="fa fa-instagram"></i><?php echo apply_filters( 'instagram_text', $instance['instagram_text'] ); ?></a>
			</li>
		<?php endif; ?>
		</ul>
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$facebook 		= ! empty( $instance['facebook'] ) ? $instance['facebook'] : esc_html__( 'Facebook link', 'filterlessco' );
		$twitter 		= ! empty( $instance['twitter'] ) ? $instance['twitter'] : esc_html__( 'Twitter link', 'filterlessco' );
		$instagram 		= ! empty( $instance['instagram'] ) ? $instance['instagram'] : esc_html__( 'Instagram link', 'filterlessco' );
		$facebook_text 	= ! empty( $instance['facebook_text'] ) ? $instance['facebook_text'] : esc_html__( 'Facebook Text', 'filterlessco' );
		$twitter_text 	= ! empty( $instance['twitter_text'] ) ? $instance['twitter_text'] : esc_html__( 'Twitter Text', 'filterlessco' );
		$instagram_text = ! empty( $instance['instagram_text'] ) ? $instance['instagram_text'] : esc_html__( 'Instagram Text', 'filterlessco' ); ?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>"><?php _e('Facebook: ', 'filterlessco'); ?></label>
			<input type="text" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'facebook' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>" value="<?php echo esc_attr( $facebook ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>"><?php _e('Twitter: ', 'filterlessco'); ?></label>
			<input type="text" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'twitter' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>" value="<?php echo esc_attr( $twitter ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>"><?php _e('Instagram: ', 'filterlessco'); ?></label>
			<input type="text" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'instagram' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>" value="<?php echo esc_attr( $instagram ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'facebook_text' ) ); ?>"><?php _e('Facebook Text: ', 'filterlessco'); ?></label>
			<input type="text" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'facebook_text' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'facebook_text' ) ); ?>" value="<?php echo esc_attr( $facebook_text ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'twitter_text' ) ); ?>"><?php _e('Twitter Text: ', 'filterlessco'); ?></label>
			<input type="text" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'twitter_text' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'twitter_text' ) ); ?>" value="<?php echo esc_attr( $twitter_text ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'instagram_text' ) ); ?>"><?php _e('Instagram Text: ', 'filterlessco'); ?></label>
			<input type="text" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'instagram_text' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'instagram_text' ) ); ?>" value="<?php echo esc_attr( $instagram_text ); ?>">
		</p>
	<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['facebook'] 		= ( ! empty( $new_instance['facebook'] ) ) ? strip_tags( $new_instance['facebook'] ) : '';
		$instance['twitter'] 		= ( ! empty( $new_instance['twitter'] ) ) ? strip_tags( $new_instance['twitter'] ) : '';
		$instance['instagram'] 		= ( ! empty( $new_instance['instagram'] ) ) ? strip_tags( $new_instance['instagram'] ) : '';
		$instance['facebook_text'] 	= ( ! empty( $new_instance['facebook_text'] ) ) ? strip_tags( $new_instance['facebook_text'] ) : '';
		$instance['twitter_text'] 	= ( ! empty( $new_instance['twitter_text'] ) ) ? strip_tags( $new_instance['twitter_text'] ) : '';
		$instance['instagram_text'] = ( ! empty( $new_instance['instagram_text'] ) ) ? strip_tags( $new_instance['instagram_text'] ) : '';

	    return $instance;
	}
}

function filterlessco_load_widget() {
	register_widget( 'Filterlessco_Social_Icons' );
}
add_action( 'widgets_init', 'filterlessco_load_widget' );
<?php
/**
 * The Template for displaying all single posts.
 *
 * @package okb
 * @since okb 1.0
 */

get_header();
  $client = get_post_meta(get_the_ID(),'client', true);
  $services = get_post_meta(get_the_ID(),'services');
?>

  <?php while ( have_posts() ) : the_post(); ?>
      <div class="project">
        <header>
          <div class='twelve'>
            <h2 class="project-title">
              <span class='label'>Project:</span>
              <?php the_title(); ?>
            </h2>
          </div>
          <div class='twelve project-client-tags'>
            <div class="three alpha">
              <h3 class="project-client">
                <span class='label'>Client:</span>
                <a href="#"><?php echo $client ? $client['post_title'] : "No Client"; ?></a>
              </h3>
            </div>
            <div class='nine omega'>
              <?php if ( $services ): ?>
                <ul class='project-services'>
                  <li class='label'>Services:</li>
                  <?php foreach ($services as $service): ?>
                    <li><?php echo project_service_archive_link($service) ?></li>
                  <?php endforeach ?>
                </ul>
              <?php endif ?>
            </div>
          </div>
        </header>
        <div class='twelve'>
          <div class="project-description">
            <?php the_content(); ?>
          </div>
        </div>
        </div>
        <div class="twelve">
          <ul class='project-images'>
            <?php
            $attachments = new Attachments( 'okb_attachments', $id );
            if( $attachments->exist() ) {
              while( $attachment = $attachments->get() ) {
                $src     = $attachments->src( 'original' );
                $video   = $attachments->field('video');
                $caption = $attachments->field('caption');

                echo "<li>";

                if ($video){ ?>
                  <div class="embedded-video-wrapper">
                    <iframe frameborder="0" height="529" width="940" src="<?php echo okb_format_and_extract_video_url($video) ?>?byline=0&amp;portrait=0&amp;badge=0&amp;color=beb4d7">webkitAllowFullScreen mozallowfullscreen allowFullScreen</iframe>
                  </div>
                <?php } else { ?>
                  <img class='scale' src='<?php echo $src ?>' />
                <?php }

                if ($caption) {
                  echo "<p class='caption'>$caption</p>";
                }

                echo "</li>";
              }
            }
            ?>
          </ul>
        </div>
        <div class='twelve'>
          <ul class="project-meta">
            <li class="site">
              <?php $url = get_post_meta(get_the_ID(),'url',true)?>
              <?php if ($url): ?>
                <a href="<?php echo $url ?>">Project Website - <?php echo $url ?></a>
              <?php else: ?>
                NO SITE
              <?php endif; ?>
            </li>
            <li class='more'>
              <?php if ($client): ?>
                <?php echo project_archive_link($client, "MORE PROJECTS FROM THIS CLIENT"); ?>
              <?php endif ?>
            </li>
            <li class="next">
              <?php echo next_post_link('%link', 'Next Project') ?>
            </li>
          </ul>
        </div>
      </div>

  <?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>


;( function( $ ) {
	$(document).ready(function(){
		if($('#primary').hasClass('representation') || $('#primary').hasClass('licensing')) {
			$('html').css('background', '#171515');
		}
		// $("a[rel^='prettyPhoto']").prettyPhoto();
	});

	// $(document).ready(function(){
	// 	$(document).on('click', '.single[class*="single-"] .col-md-4 a img', function(e){
	// 		// Stop other animations
	// 		e.preventDefault();
	// 		// remove all previous clones .col-lg-4.col-md-4
	// 		$(this).parent().parent().prevAll('.col-lg-12.col-md-12').remove();
	// 		// create a parent clone
	// 		var clone = $(this).parent().parent().clone().removeClass( "col-lg-4 col-md-4").addClass("col-lg-12 col-md-12 item");
	// 		// remove other css and classes
	// 		clone.find('img').removeClass('horizontal').css({'max-height': '100%', 'max-width': '100%', 'display': 'block', 'margin': 'auto'});
	// 		// insert the clone after %3==0 elements
	// 		// console.log($(this).parent().parent().index() %3);
	// 		switch($(this).parent().parent().index() %3) {
	// 			case 0:
	// 				$(this).parent().parent().before( clone );
	// 				$(this).parent().parent().nextAll('.col-lg-12.col-md-12').remove();
	// 				break;
	// 			case 1:
	// 				$(this).parent().parent().next().next().before( clone );
	// 				$(this).parent().parent().next().next().nextAll('.col-lg-12.col-md-12').remove();
	// 				break;
	// 			case 2:
	// 				$(this).parent().parent().next().before( clone );
	// 				$(this).parent().parent().next().nextAll('.col-lg-12.col-md-12').remove();
	// 				break;
	// 		}
	// 		// remove all next clones .col-lg-4.col-md-4
	// 		//$(this).parent().parent().nextAll('.col-lg-12.col-md-12').remove();
	// 	});
	// });

	$(document).ready(function(){
		var grid = $('.single[class*="single-"] .entry-content .col-md-12:last-of-type').isotope({
		  itemSelector: '.item',
		  masonry: {
		    columnWidth: 1
		  }
		  // percentPosition: true
		});

		grid.imagesLoaded().progress( function() {
		  grid.isotope('layout');
		});


		grid.on( 'click', '.item', function() {
			$(this).toggleClass('gigante');
		  	$(this).prevAll('.gigante').removeClass('gigante');
		  	$(this).nextAll('.gigante').removeClass('gigante');
		  	// trigger layout after item size changes

		  	grid.isotope('layout');
		  	if($(this).hasClass('gigante')) {
		  		if($('html').offset().top > $(this).offset().top)
			  		$('html,body').animate({
			    		scrollTop: $(this).offset().top},
			        	'slow'
			        );
			  	else
			  		$('html,body').animate({
			    		scrollTop: $(this).offset().top+$(this).height()/3},
			        	'slow'
			        );
		  	} else {
		  		$('html,body').animate({
			    	scrollTop: $(this).offset().top-$(this).height()-$(this).height()/2},
			        'slow');
		  	}
		});
	});
})( jQuery );
(function($){
  function zero_pad_num(num) { return num < 10 ? '0' + num.toString() : num.toString(); }

  $.extend($.okCycle.ui,{
    delayedCaption: {
      init: function(slideshow,ui,opts){
        $("<div class='credit' style='z-index:4'/>").appendTo(ui).hide();
        $.okCycle.ui.delayedCaption.setCaption(slideshow.children().eq(slideshow.data('activeSlide') || 0),$("<div class='caption' style='z-index:4' />").appendTo(ui).hide());
      },
      // If a caption begins with a octothorpe we'll consider it an id attribute of an element containing the caption
      move: function(slideshow, ui, transition){
        $.okCycle.ui.delayedCaption.setCaption(slideshow.children('.active'), $(".caption", ui));
      },
      setCaption: function(el,container){
        var caption = el.data('caption'), html = '', credit = '', creditContainer = container.closest(".okCycle-ui").find(".credit");

        if (caption) {
          html = (function(data){
            var html = "";
            if (data.caption !== '') {
              html  = '<div class="container">';
              html += '<div class="ten">' + (data.url ? "<a href='"+data.url+"'><h1><span>"+data.caption+"</span></h1></a>" :  "<h1><span>"+data.caption+"</span></h1>") + '</div>';
              html += "</div>";
            }
            return html;
          })(caption);

          if (caption.credit) {
            credit = "<div class='container'><h5 class='twelve'>"+caption.credit+"</h5></div>";
          }
        }

        if (container.is(":visible")) {
          container.add(creditContainer).stop(true,true).animate({ opacity:0 },function(){
            if (html !== '') {
              container.css({left:-300}).html(html).animate({left:0, opacity:1});
            }
            creditContainer.html(credit).animate({opacity:1});
          });
        } else {
          creditContainer.html(credit).show();
          container.html(html).show();
        }
      }
    },
    currentPage: {
      init: function(slideshow, ui, opts){
        ui.append('<ul class="current-page"><li class="current">'+(slideshow.data('active')+1)+'</li><li class="total">'+slideshow.children().length+'</li></ul>');
      },
      move: function(slideshow,ui,transition){
        $("li.current", ui).html(transition.toIndex+1);
      }
    },
    // Primarily for slideshow with stretch the width of the viewport - ensures
    // the height is correctly set to reflect the aspect ratio of the image
    fitImage:  {
      init: function(slideshow, ui, opts) {
        var win = $(window),
            src = slideshow.children().first().css('backgroundImage').match(/\s*url\(\s*["']?([^"']+)["']?\s*\)/)[1],
            img = $("<img />").hide().appendTo('body'),
            dfd = $.Deferred();

        function adjustHeight(slideshow, ih, iw) {
          var trueHeight = (ih/iw) * win.width();
          slideshow.add(slideshow.children()).css('height', opts.fitImage ? opts.fitImage(trueHeight) : trueHeight);
        }

        if (src) {
          img.attr("src", src)
          .imagesLoaded()
          .progress(function(inst, imageData){
            var h = img.height(),
                w = img.width();
                
            adjustHeight(slideshow,h,w);

            win.on('resize', function(){ adjustHeight(slideshow,h,w); });

            img.remove();
            dfd.resolve();
          });
        }
        return dfd;
      }
    }
  });
})(jQuery);



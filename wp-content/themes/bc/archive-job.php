<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package okb
 * @since okb 1.0
 */

get_header(); ?>

			<div id="content" class="twelve">

			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<h1 class="page-title">Jobs</h1>
				</header>

        <p>
        <?php echo okb_get_option('job_page_text','site_general'); ?>
        </p>

				<?php rewind_posts(); ?>

        <ul class="jobs">
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
          <li>
            <h2 class="entry-title"><?php the_title(); ?></h2>
            <?php the_content( __( '', 'okb' ) ); ?>
          </li>
				<?php endwhile; ?>
        </ul>

			<?php else : ?>

        <h2>No Current Openings</h2>

			<?php endif; ?>

			</div><!-- #content -->

<?php get_footer(); ?>

<?php
  function _okb_services($term,$class='') {
    $tabs    = '';
    $content = '';

    extract(shortcode_atts(array('type' => 'text'), $atts));  

    $services = get_posts(array(
      'post_type'   => 'service', 
      'post_status' => 'publish',
      'numberposts' => -1,
      'tax_query' => array(
        array(
          'taxonomy' => 'service_categories',
          'field' => 'slug',
          'terms' => $term
        )
      )
    ));

    foreach ($services as $service){
      if (has_post_thumbnail($service->ID)) {
        $src  = wp_get_attachment_image_src( get_post_thumbnail_id( $service->ID));
        $src  = $src[0];
        $link = $class != 'process' ? '<a class="work-link" href="'. get_post_type_archive_link( 'project' ) . '?sid=' . (get_class($service) ? $service->ID : $service['ID']) .'">See '. $service->post_title .' work</a> ' : '';

        $slug     = strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', $service->post_title));
        $text     = apply_filters('the_content', $service->post_content);
        $tabs    .= "<li class='$slug'><a href='#$slug' style='background-image:url($src)'>$slug</a></li>";
        $content .= "<li id='$slug'><h1 class='service-title'>$service->post_title</h1>$text$link</li>";
      }
    }

    return "<ul class='$class services tabs'>$tabs</ul><ul class='tab-content'>$content</ul>";
  }

  function okb_services($atts, $content = null) {  
    return _okb_services('service');
  }  

  function okb_process($atts, $content = null) {  
    return _okb_services('process', 'process');
  }  

  add_shortcode("services", "okb_services");  
  add_shortcode("process", "okb_process");


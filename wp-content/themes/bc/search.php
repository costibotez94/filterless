<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package okb
 * @since okb 1.0
 */

get_header(); ?>

			<?php if ( have_posts() ) : ?>

				<header class="page-header twelve">
					<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'okb' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				</header>

        <ul class="entries nine infinite-scrolling">
				<?php while ( have_posts() ) : the_post(); ?>

          <?php echo okb_partial('post') ?>

				<?php endwhile; ?>
        </ul>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'search' ); ?>

			<?php endif; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

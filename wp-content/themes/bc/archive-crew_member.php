<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package okb
 * @since okb 1.0
 */

get_header();

$crews = get_posts(array(
  'post_type'   => 'crew_member',
  'post_status' => 'publish',
  'numberposts' => -1,
));

?>
  <ul class="crew">
  <?php foreach ($crews as $post): ?>
    <?php echo okb_partial('project') ?>
  <?php endforeach ?>
  </ul>

<?php get_footer(); ?>

/**
 * jquery.miniScroll.js
 *
 * Copyright (c) 2012 Asher Van Brunt | http://www.okbreathe.com
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 * Date: 10/15/12
 *
 * @description Compact endless Scrolling
 * @author Asher Van Brunt
 * @mailto asher@okbreathe.com
 * @version 0.10
 *
 */
(function($){

  function MiniScroll(element,opts) {
    this.opts = $.extend({
      buffer     : 50, // Pixels from the bottom
      onFinish   : function(){ $(this.element).append("<em>No more results</em>"); }, // Called when we no longer can retrieve any more reults
      onLoad     : function(){}, // Called before AJAX request
      onComplete : function(xhr, status){}, // Ajax complete callback
      onSuccess  : function(data, status, xhr){}, // Ajax success callback
      onError    : function(xhr, status, error){}, // Ajax error callback
      dataType   : null, // What type of data the server returns
      url        : function(page){ return location.pathname + "?page=" + page; } // Where to look for content
    }, opts);

    this.element = element;

    this.state   = {
      isDuringAjax: false,
      isError: false,
      isFinished: false, // When we've reached the end of the results 
      currPage: 1
    };

    this.bindScrollEvent();
  }

  $.extend(MiniScroll.prototype,{
    bindScrollEvent: function(){
      var self = this;
      $(this.element).bind('scroll', function(e){ self.scroll(e); });
    },
    reset: function(){
      this.state.currPage = 1;
      this.state.isDuringAjax = this.state.isError = this.state.isFinished = false;
      this.bindScrollEvent();
    },
    error: function(xhr, status, error){
      switch ( xhr.status ) {
        case 404:
          this.state.isFinished = true;  
          this.opts.onFinish.call(this, xhr, status, error);
          break;
        default:
          this.state.isError = true;
          this.opts.onError.call(this, xhr, status, error);
          break;
      }

      $(this.element).unbind('scroll');
    },
    scroll: function(){
      var self      = this,
          canScroll = false,
          wrapper;

      if (this.element == document) {
        canScroll = $(document).height() - $(window).height() <= $(window).scrollTop() + this.opts.buffer;
      } else {
        // calculate height of the scrolling container
        wrapper = $(".miniscoll-inner-wrap", this.element);

        if (wrapper.length === 0) {
          $(this.element).wrapInner("<div class='miniscoll-inner-wrap' />");
        }

        canScroll = wrapper.length > 0 && (wrapper.height() - $(this.element).height() <= $(this.element).scrollTop() + this.opts.buffer);
      }

      if (canScroll && !( this.state.isFinished || this.state.isError || this.state.isDuringAjax ) ) {

        this.state.currPage++;

        self.state.isDuringAjax = true; 

        this.opts.onLoad.call(this);

        $.ajax({
          url      : self.opts.url.call(self, self.state.currPage),
          dataType : self.opts.dataType,
          type     : 'GET',
          complete : function(xhr,status){ 
            self.state.isDuringAjax = false; 
            self.opts.onComplete.call(self,xhr,status);
          },
          success  : function(data,status,xhr){ self.opts.onSuccess.call(self, data,status,xhr); },
          error    : function(xhr,status,error){ self.error(xhr,status,error); }
        });

      }
    }
  });

  $.fn.miniScroll = function(opts){

    return this.each(function(){
      $(this).data('miniScroll', new MiniScroll(this,opts)); 
    }).bind("miniScroll.reset", function(){
      $(this).data('miniScroll').reset();
    });

  };

})(jQuery);

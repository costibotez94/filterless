<?php
/**
 * FilterlessCo functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @author Botez Costin <costibotez94@gmail.com>
 * @package FilterlessCo
 */

add_filter( 'jpeg_quality', 'filterless_custom_jpeg_quality' );
function filterless_custom_jpeg_quality( $quality ) { 
   return 100;
}

if ( ! function_exists( 'filterlessco_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function filterlessco_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on FilterlessCo, use a find and replace
	 * to change 'filterlessco' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'filterlessco', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'filterlessco' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'filterlessco_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Set up image size for Projects
	add_image_size( 'projects-size', 280, 180, true );
	add_image_size( 'artists-size', 280, 180, true );
	add_image_size( 'represenation-size', 270, 174, true );
}
endif;
add_action( 'after_setup_theme', 'filterlessco_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function filterlessco_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'filterlessco_content_width', 640 );
}
add_action( 'after_setup_theme', 'filterlessco_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function filterlessco_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'filterlessco' ),
		'id'            => 'footer-social-icons',
		'description'   => esc_html__( 'Add widgets here.', 'filterlessco' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'filterlessco_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function filterlessco_scripts() {
	wp_enqueue_style( 'filterlessco-style', get_stylesheet_uri() );
	wp_enqueue_style( 'filterlessco-fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_script( 'filterlessco-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'filterlessco-script', get_template_directory_uri() . '/js/script.js', array());
	wp_enqueue_script( 'filterlessco-imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array());
	wp_enqueue_script( 'filterlessco-isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array());
	wp_enqueue_script( 'filterlessco-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }
}
add_action( 'wp_enqueue_scripts', 'filterlessco_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Social Icons class file.
 */
require get_template_directory() . '/inc/class.social_icons.php';

/**
 * Paginate posts in loop
 * @version 1.1
 */
function costin_pagination($query, $pages = '', $range = 2) {
		$showitems = ($range * 2)+1;

		global $paged;
		if(empty($paged)) $paged = 1;

		if($pages == '') {
		 	$pages = $query->max_num_pages;
		 	if(!$pages)
						$pages = 1;
		}

		if(1 != $pages) {
		 	echo "<div class='pagination'>";
					echo "<a href='".get_pagenum_link($paged - 1)."'>&laquo; Previous</a>";

				for ($i=1; $i<=$pages; $i++) {
						if (1 != $pages ) {
								echo ($paged == $i)? "<a class='current'>".$i."</a>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
						}
				}
				if($paged<$pages)
					echo "<a href='".get_pagenum_link($paged + 1)."'>Next &raquo;</a>";
				else
					echo "<a href='".get_pagenum_link($paged)."'>Next &raquo;</a>";
				echo "</div>";
		}
}

function post_options($type = 'post'){
	$query = new WP_Query(array(
			'post_type'      	=> $type,
			'posts_per_page' 	=> -1,
			'post_status'		=> 'publish',
			'nopaging'       	=> true,
		));
		$posts =  $query->posts;

		$ret  = array('' => '');
		foreach ($posts as $post) {
			$ret[$post->ID] =  $post->post_title;
		}

		return $ret;
}

function okb_attachments( $attachments ) {
	$fields         = array(
		array(
			'name'      => 'caption',
			'type'      => 'textarea',
			'label'     => __( 'Caption', 'attachments' ),
			'default'   => '',
		),
		// array(
		//   'name'      => 'bg_color',
		//   'type'      => 'text',
		//   'label'     => __( 'Background Color', 'attachments' ),
		//   'default'   => 'rgba(255,255,255,1)',
		// ),
		array(
			'name'      => 'page_id',
			'type'      => 'select',
			'label'     => __( 'Page Link', 'attachments' ),
			'meta'      => array(
				'multiple'      => false,
				'options'       => post_options(array('artists','project','post'))
			),
		),
		array(
			'name'      => 'url',
			'type'      => 'text',
			'label'     => __( 'External URL', 'attachments' ),
		),
		array(
			'name'      => 'video',
			'type'      => 'text',
			'label'     => __( 'Video Embed URL', 'attachments' ),
			'desc'      => 'This will override the image if both are set'
		),
		array(
			'name'      => 'credit',
			'type'      => 'text',
			'label'     => __( 'Photo Credit', 'attachments' ),
		),
	);

	$args = array(
		// title of the meta box (string)
		'label'         => 'Slides',

		// all post types to utilize (string|array)
		'post_type'     => array( 'slideshow', 'project', 'artists', 'post' ),

		// meta box position (string) (normal, side or advanced)
		'position'      => 'normal',

		// meta box priority (string) (high, default, low, core)
		'priority'      => 'high',

		// allowed file type(s) (array) (image|video|text|audio|application)
		'filetype'      => array('image'),

		// by default new Attachments will be appended to the list
		// but you can have then prepend if you set this to false
		'append'        => true,

		// text for 'Attach' button in meta box (string)
		'button_text'   => __( 'Attach Files', 'attachments' ),

		// text for modal 'Attach' button (string)
		'modal_text'    => __( 'Attach', 'attachments' ),

		// which tab should be the default in the modal (string) (browse|upload)
		'router'        => 'browse',

		// fields array
		'fields'        => $fields,

	);

	$attachments->register( 'okb_attachments', $args ); // unique instance name

}

add_action( 'attachments_register', 'okb_attachments' );
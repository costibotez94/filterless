<?php

$services = get_posts(array(
  'post_type'   => 'service',
  'post_status' => 'publish',
  'sort_order'  => 'desc',
  'sort_column' => 'post_title',
  'numberposts' => -1,
  'tax_query' => array(
      array(
        'taxonomy' => 'service_categories',
        'field' => 'id',
        'terms' => 3
      )
    ),
));

?>

<div class="filter twelve">
  <ul>
    <li><a href="<?php echo get_post_type_archive_link( 'project' ); ?>">All</a></li>
  <?php foreach ($services as $service): ?>
    <li><?php echo project_service_archive_link($service) ?></li>
  <?php endforeach ?>
  </ul>
</div>

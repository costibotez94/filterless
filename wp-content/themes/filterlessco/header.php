<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @author Botez Costin <costibotez94@gmail.com>
 * @package FilterlessCo
 */

?><!DOCTYPE html>

<?php if(is_page('home')): ?>
	<style>
		@media (max-width: 1200px) {
			html {
				background-position: -100px 70px;
			}
		}
		@media (max-width: 980px) {
			html {
				background-position: -160px 70px;
			}
		}
		@media (max-width: 800px) {
			html {
				background-position: -250px 70px;
			}
		}
		@media (max-width: 710px) {
			html {
				background-position: -300px 40px;
			}
		}
		@media (max-width: 610px) {
			html {
				background-position: -350px 40px;
			}
		}
		@media (max-width: 510px) {
			html {
				background-position: -400px 40px;
			}
		}
		@media (max-width: 450px) {
			html {
				background-position: -500px 49px;
			}
		}
	</style>
	<html
		<?php language_attributes(); ?> style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/home.jpg);
		background-repeat: no-repeat; background-attachment: fixed;">
<?php else: ?>
	<html <?php language_attributes(); ?>>
<?php endif; ?>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.js"
  integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php wp_head(); ?>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<?php global $post; ?>
<body <?php body_class($post->post_name); ?>>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-51088-42', 'auto');
  ga('send', 'pageview');
</script>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<div class="container">
			<nav class="navbar">
		  		<div class="container-fluid">
			    	<!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
				        	<span class="sr-only">Toggle navigation</span>
				        	<span class="icon-bar"></span>
				        	<span class="icon-bar"></span>
				        	<span class="icon-bar"></span>
				      	</button>
				      	<a class="navbar-brand" href="<?php echo home_url(); ?>">
				      		<img src="<?php echo home_url('/wp-content/themes/filterlessco/inc/main-logo.png'); ?>" alt="logo"/>
				      	</a>
				    </div>

			    	<!-- Collect the nav links, forms, and other content for toggling -->
			    	<div class="collapse navbar-collapse" id="navbar-collapse">
				      	<?php
				      		wp_nav_menu( array(
				      			'theme_location' 	=> 'primary',
				      			'menu_id' 			=> 'primary-menu',
				      			'menu_class'		=> 'nav navbar-nav'
				      		));
				      	?>
			    	</div><!-- /.navbar-collapse -->
		  		</div><!-- /.container-fluid -->
			</nav>
		</div>
	</header><!-- #masthead -->



	<div id="content" class="site-content">

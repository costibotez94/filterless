<?php
/**
 * @package okb
 * @since okb 1.0
 */

  $images = get_attachments_src(get_the_ID(),'full');
  if (count($images) == 0) {
    $attachments = new Attachments( 'attachments' );
  }
?>

<div class="entry twelve blog_single">
    <?php // for some reason some posts have the attachments set different, probably just need to reupload ?>
    <header>
      <div class='row'>
        <div class='ten alpha'>
          <h2 class="entry-title"><?php the_title(); ?></h2>
        </div>
        <div class='two omega next-entry'>
          <?php next_post_link("%link","Next Post"); ?>
        </div>
      </div>
      <div class='row'>
        <div class='four alpha'>
          <h3 class="entry-meta">
            <span class="entry-date"><?php okb_posted_on(); ?></span> By <span class="entry-author"><?php echo get_the_author() ?></span>
          </h3>
        </div>
        <div class='eight omega'>
          <?php echo get_the_tag_list('<ul class="entry-tags"><li>','</li><li>','</li></ul>'); ?>
        </div>
      </div>
      <div class='row'>
        <ul class="twelve entry-social alpha omega">
          <li>
            <a href="//pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink()); ?>&description=<?php echo urlencode(get_the_title()); ?>" data-pin-do="buttonBookmark" >
              <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
            </a>
            <script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
          </li>
          <li>
            <div class="fb-like" data-href="<?php echo get_permalink() ?>" data-send="false" data-layout="button_count" data-width="200" data-show-faces="false"></div>
          </li>
          <li class='tweet'>
            <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php the_title(); ?>" data-width="80px">Tweet</a>
                  <script>!function(d,s,id){
                    var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
                    if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}
                  }(document, 'script', 'twitter-wjs');</script>
          </li>
          <li>
            <a href="http://www.tumblr.com/share" title="Share on Tumblr" style="display:inline-block; text-indent:-9999px; overflow:hidden; width:81px; height:20px; background:url('http://platform.tumblr.com/v1/share_1.png') top left no-repeat transparent;">Share on Tumblr</a>
          </li>
        </ul>
      </div>
    </header>
    <?php
      if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
        the_post_thumbnail('full', array('class' => 'scale'));
      }
    ?>
    <?php if (count($images)) : ?>

        <ul>
          <?php foreach ($images as $image): ?>
            <li><img class="scale" alt="<?php the_title(); ?>" src="<?php echo $image ?>" /></li>
          <?php endforeach ?>
        </ul>

    <?php endif; ?>

    <?php if( $attachments->exist() ) : ?>
      <ul>
      <?php while( $attachments->get() ) : ?>
        <li>
        <?php echo $attachments->image( 'full' ); ?>
        </li>
        <?php endwhile; ?>
      </ul>
    <?php endif; ?>

  <div class="entry-content twelve alpha">
		<?php the_content(); ?>
		<?php edit_post_link( __( 'Edit', 'okb' ), '<span class="edit-link">', '</span>' ); ?>
  </div>
</div>

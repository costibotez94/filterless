<?php
/**
 * The template for displaying Contact page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @author Botez Costin <costibotez94@gmail.com>
 * @package FilterlessCo
 */

get_header(); ?>

	<div id="primary" class="content-area contact-us">
		<main id="main" class="site-main container">
			<h1><?php the_title(); ?></h1>
			<div class="row">
				<div class="col-md-6">
					<?php global $post; echo do_shortcode($post->post_content); ?>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="contact-details">
					<?php if(!empty($email = get_field( "email" ))) : ?>
						<p>
							<a href="mailto:info@filterless.co">info@filterless.co</a>
							<!--
							<a href="mailto:<?php echo $email; ?>"><?php echo __("Contact ", 'filterlessco') . $email; ?></a>
							-->
						</p>
					<?php endif; ?>
					<!--
					<?php if(!empty($phone = get_field( "phone" ))) : ?>
						<p>
							<a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
						</p>
					<?php endif; ?>
					-->
					</div>
					<!-- <img src="<?php //echo the_post_thumbnail_url(); ?>" alt="contact-img"/> -->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();

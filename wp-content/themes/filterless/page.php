<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package	WordPress
 * @subpackage	Timber
 * @since		Timber 0.1
 */

require_once('models.php');

$context = Timber::get_context();
$post = new TimberPost();

$context['post'] = $post;

if ($post->slug == 'representation') {
	$artists = Timber::get_posts(array('post_type' => 'artists'), 'Artist');
	$context['artists'] = $artists;
	Timber::render(array('representation.twig'), $context);
} elseif ($post->slug == 'production') {
	$context['projects'] = Timber::get_posts(
		array(
			'post_type' => 'project',
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'project_tags',
					'field' => 'slug',
					'terms' => array('pre-production', 'production', 'post-production'),
				)
			)
		),
	'Project');
	Timber::render(array('production.twig'), $context);
} elseif ($post->slug == 'licensing') {
	$context['projects'] = Timber::get_posts(
		array(
			'post_type' => 'project',
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'project_tags',
					'field' => 'slug',
					'terms' => array('licensing'),
				)
			)
		),
	'Project');
	Timber::render(array('licensing.twig'), $context);
} elseif ($post->slug == 'work') {
	global $paged;
	if (!isset($paged) || !$paged) {
		$paged = 1;
	}
	$args = array(
		'post_type' => 'project',
		'posts_per_page' => 6,
		'paged' => $paged,
	);
	query_posts($args);
	$context['projects'] = Timber::get_posts($args, 'Project');
	$context['pagination'] = Timber::get_pagination();
	Timber::render(array('work.twig'), $context);
} else {
	Timber::render(array('page-' . $post->post_name . '.twig', 'page.twig' ), $context);
}

<?php
/**
 * http://wordpress.org/extend/plugins/settings-api/
 */

require( get_template_directory() . '/inc/settings-api/class.settings-api.php' );


/**
 * Registers settings section and fields
 */
function okb_admin_init() {

    $sections = array(
        array(
            'id' => 'site_general',
            'title' => __( 'Site Settings', 'okb' )
        ),
        array(
            'id' => 'contact_info',
            'title' => __( 'Contact Information', 'okb' )
        ),
        array(
            'id' => 'social',
            'title' => __( 'Social Media', 'okb' )
        ),
    );

    $fields = array(
        'site_general' => array(
            array(
                'name'    => 'home_slideshow',
                'label'   => __( 'Home Slideshow', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'select',
                'options' => post_options('slideshow')
            ),
            array(
                'name'    => 'crew_slideshow',
                'label'   => __( 'Crew Slideshow', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'select',
                'options' => post_options('slideshow')
            ),
            array(
                'name'    => 'event_page',
                'label'   => __( 'Event Page', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'select',
                'options' => page_options()
            ),
            array(
                'name'    => 'contact_page',
                'label'   => __( 'Contact Page', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'select',
                'options' => page_options()
            ),
            array(
                'name'    => 'blog_page_text',
                'label'   => __( 'Blog Page Text', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'textarea',
            ),
            array(
                'name'    => 'client_page_text',
                'label'   => __( 'Client Page Text', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'textarea',
            ),
            array(
                'name'    => 'job_page_text',
                'label'   => __( 'Job Page Text', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'textarea',
            ),
        ),
        'contact_info' => array(
            array(
                'name'    => 'mission_statement',
                'label'   => __( 'Mission Statement', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'textarea',
            ),
            array(
                'name'    => 'phone',
                'label'   => __( 'Phone', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'text',
                'default' => '',
            ),
            array(
                'name'    => 'address',
                'label'   => __( 'Address', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'textarea',
                'default' => '',
            ),
            array(
                'name'    => 'gmap',
                'label'   => __( 'Google Maps Link', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'text',
                'default' => '',
            ),
            array(
                'name'    => 'filterless',
                'label'   => __( 'Filterless', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'text',
                'default' => '',
            ),
        ),
        'social' => array(
            array(
                'name'    => 'facebook',
                'label'   => __( 'FaceBook', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'text',
                'default' => 'http://facebook.com',
            ),
            array(
                'name'    => 'twitter',
                'label'   => __( 'Twitter', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'text',
                'default' => 'http://twitter.com',
            ),
            array(
                'name'    => 'linkedin',
                'label'   => __( 'LinkedIn', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'text',
                'default' => 'http://linkedin.com',
            ),
            array(
                'name'    => 'instagram',
                'label'   => __( 'Instagram', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'text',
                'default' => 'http://instagram.com',
            ),
            array(
                'name'    => 'vimeo',
                'label'   => __( 'Vimeo', 'okb' ),
                'desc'    => __( '', 'okb' ),
                'type'    => 'text',
                'default' => 'http://vimeo.com',
            ),
        )
    );

    $settings_api = WeDevs_Settings_API::getInstance();

    //set sections and fields
    $settings_api->set_sections( $sections );
    $settings_api->set_fields( $fields );

    //initialize them
    $settings_api->admin_init();
}

add_action( 'admin_init', 'okb_admin_init' );


function cat_options($tax_name){
  $cats = get_categories(array('taxonomy' => $tax_name));
  $ret  = array('' => '');
  foreach ($cats as $cat) {
    $ret[$cat->term_id] =  $cat->name;
  }
  return $ret;
}

function page_options() {
  $pages = get_pages();
  $ret  = array('' => '');
  foreach ($pages as $page) {
    $ret[$page->ID] =  $page->post_title;
  }
  return $ret;
}

function menu_options($menu_name = 'primary'){
  $ret  = array('' => '');

  if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    foreach ( (array) $menu_items as $key => $menu_item ) {
      if ($menu_item->title != 'Spacer') {
        $ret[$menu_item->url] = $menu_item->title;
      }
    }
  }
  return $ret;
}

function post_options($type = 'post'){
  $query = new wp_query(array(
    'post_type'      => $type,
    'posts_per_page' => -1,
    'post_status'=>'publish',
    'nopaging'       => true,
  ));
  $posts =  $query->posts;
  $ret  = array('' => '');
  foreach ($posts as $post) {
    $ret[$post->ID] =  $post->post_title;
  }
  return $ret;
}

/**
 * Register the plugin page
 * http://codex.wordpress.org/Function_Reference/add_options_page
 */
function okb_admin_menu() {
  add_options_page( 'Butchershop', 'Butchershop', 'delete_posts', 'site_settings', 'site_settings_page' );
}

add_action( 'admin_menu', 'okb_admin_menu' );

/**
 * Display the plugin settings options page
 */
function site_settings_page() {
  $settings_api = WeDevs_Settings_API::getInstance();

  echo '<div class="wrap">';

  settings_errors();

  $settings_api->show_navigation();
  $settings_api->show_forms();

  echo '</div>';
}


/**
 * Get the value of a settings field
 *
 * @param string $option settings field name
 * @param string $section the section name this field belongs to
 * @param string $default default text if it's not found
 * @return mixed
 */
function okb_get_option( $option, $section, $default = '' ) {

    $options = get_option( $section );

    if ( isset( $options[$option] ) ) {
        return $options[$option];
    }

    return $default;
}



/* Attachments */

function okb_attachments( $attachments ) {
  $fields         = array(
    array(
      'name'      => 'caption',
      'type'      => 'textarea',
      'label'     => __( 'Caption', 'attachments' ),
      'default'   => '',
    ),
    // array(
    //   'name'      => 'bg_color',
    //   'type'      => 'text',
    //   'label'     => __( 'Background Color', 'attachments' ),
    //   'default'   => 'rgba(255,255,255,1)',
    // ),
    array(
      'name'      => 'page_id',
      'type'      => 'select',
      'label'     => __( 'Page Link', 'attachments' ),
      'meta'      => array(
        'multiple'      => false,
        'options'       => post_options(array('artists','project','post'))
      ),
    ),
    array(
      'name'      => 'url',
      'type'      => 'text',
      'label'     => __( 'External URL', 'attachments' ),
    ),
    array(
      'name'      => 'video',
      'type'      => 'text',
      'label'     => __( 'Video Embed URL', 'attachments' ),
      'desc'      => 'This will override the image if both are set'
    ),
    array(
      'name'      => 'credit',
      'type'      => 'text',
      'label'     => __( 'Photo Credit', 'attachments' ),
    ),
  );

  $args = array(
    // title of the meta box (string)
    'label'         => 'Slides',

    // all post types to utilize (string|array)
    'post_type'     => array( 'slideshow', 'project', 'artists', 'post' ),

    // meta box position (string) (normal, side or advanced)
    'position'      => 'normal',

    // meta box priority (string) (high, default, low, core)
    'priority'      => 'high',

    // allowed file type(s) (array) (image|video|text|audio|application)
    'filetype'      => array('image'),

    // by default new Attachments will be appended to the list
    // but you can have then prepend if you set this to false
    'append'        => true,

    // text for 'Attach' button in meta box (string)
    'button_text'   => __( 'Attach Files', 'attachments' ),

    // text for modal 'Attach' button (string)
    'modal_text'    => __( 'Attach', 'attachments' ),

    // which tab should be the default in the modal (string) (browse|upload)
    'router'        => 'browse',

    // fields array
    'fields'        => $fields,

  );

  $attachments->register( 'okb_attachments', $args ); // unique instance name

}

add_action( 'attachments_register', 'okb_attachments' );

function okb_add_admin_scripts() {
	wp_enqueue_style( 'spectrumcss', get_template_directory_uri() . '/css/spectrum.css' );
  wp_enqueue_script( 'spectrumjs', get_stylesheet_directory_uri() . "/js/lib/spectrum.js", array('jquery', 'media-upload') );
  wp_enqueue_script( 'color-admin', get_stylesheet_directory_uri() . "/js/admin.js", array('jquery', 'media-upload', 'spectrumjs') );
}

add_action('admin_init', 'okb_add_admin_scripts');

